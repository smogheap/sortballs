/*
  "sortballs" by Owen Swerkstrom  https://gitlab.com/smogheap/sortballs
  license: GPL3 or CC BY-SA
  silly command-line version of an already silly game.
  seems to work on linux, probably needs some work to run elsewhere.
*/


#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>


enum color {
	empty = 0,
	red = 31,
	green,
	yellow,
	blue,
	magenta,
	cyan,
	white,
	brown
};
int W = 8;
int H = 4;
int COMPACT = 0;
int COLORBLIND = 0;


enum color pickball(enum color **grid, int x, int peek) {
	int y;
	enum color found = empty;

	for(y = 0; y < H; y++) {
		found = grid[y][x];
		if(found) {
			if(!peek) {
				grid[y][x] = empty;
			}
			break;
		}
	}
	return found;
}
int dropball(enum color **grid, int x, enum color col) {
	int y;
	enum color found = empty;

	for(y = H - 1; y >= 0; y--) {
		if(!grid[y][x]) {
			grid[y][x] = col;
			return 0;
		}
	}
	return -1;
}

int tapcolumn(enum color **grid, enum color *hold, int x, int cheat) {
	if(!*hold) {
		*hold = pickball(grid, x, 0);
		return 0;
	} else {
		if(cheat || !pickball(grid, x, 1) || pickball(grid, x, 1) == *hold) {
			/* sorting or empty or same color */
			if(!dropball(grid, x, *hold)) {
				*hold = empty;
				return 0;
			}
		}
	}
	return -1;
}

void ball(enum color col) {
	char b = 'o';

	if (COLORBLIND) {
		b = 'A' + (((int) (col - red)) % 26);
	}

	if(brown == col) {
		printf("\x1B[%dm%c\x1B[0m", yellow, b);
	} else if(col) {
		printf("\x1B[%dm\x1B[1m%c\x1B[0m", col, b);
	} else {
		printf(" ");
	}
	return;
}

void render(enum color **grid, int hold) {
	int x;
	int y;

	//printf("\x1B[2J"); //reset
	//printf("\x1B[2J\x1B[H"); //reset and go home

	printf("[q]uit  [s]huffle");
	if(COMPACT) {
		printf("\n");
	} else {
		printf("  ");
	}
	printf("[u]ndo  [r]etry\n");

	if(!COMPACT) {
		printf("\n");
	}
	if(COMPACT) {
		printf("%*s", W, "hold: (");
	} else {
		printf("%*s", (2 * W) - 1, "hold: (");
	}
	ball(hold);
	printf(")\n");
	if(!COMPACT) {
		printf("\n");
	}
	/*
	if(!COMPACT) {
		printf("[1");
		for(x = 1; x < W; x++) {
			printf("   %d", (x + 1) % 10);
		}
		printf("]\n");
	}
	*/
	for(y = 0; y < H; y++) {
		for(x = 0; x < W; x++) {
			printf("|");
			ball(grid[y][x]);
			if(!COMPACT) {
				printf("| ");
			}
		}
		if(COMPACT) {
			printf("|");
		}
		printf("\n");
	}
	printf("\x1B[K"); // clear current line (in case win msg overflowed)
	printf("[1");
	for(x = 1; x < W; x++) {
		if(!COMPACT) {
			printf("  ");
		}
		printf(" %d", (x + 1) % 10);
	}
	printf("]\n");
	return;
}
void reposition() {
	printf("\x1B[H");
	//printf("\x1B[F\x1B[%dA", H + (COMPACT ? 3 : 4));
	return;
}

void burp(enum color **grid, enum color *hold) {
	int head = 0;
	int tail = W - 1;

	if(*hold) {
		while(dropball(grid, head, *hold)) {
			head++;
		}
		*hold = empty;
	}
	while(!pickball(grid, tail, 1)) {
		tail--;
	}
	while(head < tail && pickball(grid, tail, 1)) {
		tapcolumn(grid, hold, tail, 1);
		while(dropball(grid, head, *hold)) {
			head++;
		}
		*hold = empty;
		//tapcolumn(grid, hold, head, 1);
		if(!pickball(grid, tail, 1)) {
			tail--;
		}
	}
}

void shuffle(enum color **grid, enum color *hold) {
	int i;
	for(i = 0; i < 100; i++) {
		tapcolumn(grid, hold, rand() % W, 1);
	}
}
void shuffle_anim(enum color **grid, enum color *hold) {
	int i;
	for(i = 0; i < 100; i++) {
		shuffle(grid, hold);
		reposition();
		render(grid, *hold);
		usleep(10000);
	}
	burp(grid, hold);
}

int undo(enum color **grid, enum color *hold, int prev) {
	if(prev) {
		tapcolumn(grid, hold, prev, 1);
		prev = 0;
	}
	return prev;
}

int checkwin(enum color **grid) {
	int y;
	int x;
	enum color prev = empty;

	printf("\x1B[K"); // clear current line (in case win msg overflowed)
	for(x = 0; x < W; x++) {
		for(y = 0; y < H; y++) {
			if(y && prev != grid[y][x]) {
				return 0;
			}
			prev = grid[y][x];
		}
	}
	return 1;
}
void win() {
	const char *message[] = {
		"Success!", "Hooray!", "Victory!", "Bla-DOW!", "Kablammo!", "Boom!",
		"Yesss!", "w00t w00t!", "A WINNER IS YOU", "Wub-a-lub-a-dub DUB!"
	};
	const int max = (sizeof(message) / sizeof(char *));
	printf("%s\n", message[rand() % max]);
}


char getch() {
	char ch;
	struct termios old;
	struct termios nobuf;
	tcgetattr(0, &old);
	nobuf = old;
	nobuf.c_lflag &= ~(ICANON | ECHO); /* no buffered i/o or echo */
	tcsetattr(0, TCSANOW, &nobuf);
	ch = getchar();
	tcsetattr(0, TCSANOW, &old);
	return ch;
}
void copygrid(enum color **to, enum color **from) {
	int x;
	int y;
	for(y = 0; y < H; y++) {
		for(x = 0; x < W; x++) {
			to[y][x] = from[y][x];
		}
	}
}


int main(int argc, char **argv) {
	enum color **grid;
	enum color **backup;
	enum color hold = empty;
	char input = '\0';
	int x;
	int y;
	int prev;
	int noshuffle = 0;
	int quit = 0;
	int help = 0;

	y = 0;
	for(x = 1; argv[x]; x++) {
		if(!strcmp("-c", argv[x]) || !strcmp("--compact", argv[x])) {
			COMPACT = 1;
			continue;
		} else if(!strcmp("-C", argv[x]) || !strcmp("--colorblind", argv[x])) {
			COLORBLIND = 1;
			continue;
		} else if(!strcmp("-s", argv[x]) || !strcmp("--shuffle", argv[x])) {
			noshuffle = 1;
			continue;
		} else if(!strcmp("-w", argv[x]) || !strcmp("--win", argv[x])) {
			quit = 1;
			continue;
		} else if(atoi(argv[x])) {
			if(y) {  /* done this before, second number */
				H = atoi(argv[x]);
				if(H < 3) H = 3;
				if(H > 12) H = 12;
			} else {
				W = atoi(argv[x]);
				if(W < 4) W = 4;
				if(W > 10) W = 10;
				y++;
			}
			continue;
		} else {
			help = 1;
		}
	}
	if(help) {
		printf("usage: %s [OPTION]... [BINS 4-10] [DEPTH 3-10]\n", argv[0]);
		printf("play a ball-sorting game.  (default: 8 bins of depth 4)\n");
		printf("\n");
		printf("  -c --compact    display as small as possible\n");
		printf("  -C --colorblind display different letters for each color\n");
		printf("  -s --shuffle    skip auto-shuffle\n");
		printf("  -w --win        quit after winning\n");
		printf("  -h --help       display this help\n");
		printf("\n");
		printf("homepage: https://gitlab.com/smogheap/sortballs\n");
		return 0;
	}

	srand(time(NULL));
	grid = calloc(H, sizeof(enum color*));
	backup = calloc(H, sizeof(enum color*));
	for(y = 0; y < H; y++) {
		grid[y] = calloc(W, sizeof(enum color));
		backup[y] = calloc(W, sizeof(enum color));
		for(x = 0; x < W; x++) {
			if(W - 2 > x) {
				grid[y][x] = red + x;
			} else {
				grid[y][x] = empty;
			}
		}
	}

	/* save terminal contents */
	printf("\x1B[?47h");

	printf("\x1B[2J\x1B[H"); //reset and go home
	if(!noshuffle) {
		render(grid, hold);
		shuffle_anim(grid, &hold);
		reposition();
	}
	render(grid, hold);
	copygrid(backup, grid);
	while((input = getch())) {
		if(input == 'q' || input == -1) {
			break;
		}
		if(input == '\n') {
			continue;
		}
		if(input == 's') {
			printf("\x1B[2J\x1B[H"); //reset and go home
			shuffle_anim(grid, &hold);
			copygrid(backup, grid);
		}
		if(input == 'r') {
			copygrid(grid, backup);
			hold = empty;
		}
		if(input == 'u') {
			prev = undo(grid, &hold, prev);
		}
		if(input == '0' && 10 == W) {
			if(!tapcolumn(grid, &hold, 9, 0)) {
				prev = 9;
			}
		}
		if(input >= '1' && input <= '9' && input - '1' < W) {
			if(!tapcolumn(grid, &hold, input - '1', 0)) {
				prev = input - '1';
			}
		}
		reposition();
		render(grid, hold);

		if(checkwin(grid)) {
			win();
			if(quit) {
				break;
				//printf("\x1B[F\x1b[K");
			}
		}
	}

	/* restore terminal contents */
	printf("\x1B[?47l");
	if(quit && checkwin(grid)) {
		win();
	}

	for(y = 0; y < H; y++) {
		free(grid[y]);
		free(backup[y]);
	}
	free(grid);
	free(backup);
	return 0;
}
